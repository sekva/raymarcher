use crate::marchador::*;
use crate::ponto::*;
use crate::util::*;

#[derive(Copy, Clone, Debug)]
pub struct Camera {
    pos: Ponto,
    up: Ponto,
    frente: Ponto,
    direita: Ponto,
    teta: f64, //horizontal
    phi: f64,  //vertical
    focal: f64,
}

impl Camera {
    pub fn nova() -> Camera {
        let pos = Ponto::novo(0.0, 2.0, 4.0);
        let focal = 1.8 as f64;
        let teta = 0 as f64;
        let phi = 0 as f64;

        let seno_teta = teta.sin();
        let cos_teta = teta.cos();
        let seno_phi = phi.sin();
        let cos_phi = phi.cos();

        let frente = Ponto::novo(cos_phi * seno_teta, -seno_phi, cos_phi * cos_teta);
        let direita = Ponto::novo(cos_teta, 0.0, -seno_teta);
        let up = cross(frente, direita).normalizar(1.0);

        println!("up: {:?}", up);
        println!("frente: {:?}", frente);
        println!("direita: {:?}", direita);

        Camera {
            pos,
            up,
            frente,
            direita,
            teta,
            phi,
            focal,
        }
    }

    pub fn gerar_marchador(&self, x: usize, y: usize, width: usize, height: usize) -> Marchador {
        let xx = x as f64 / width as f64;
        let yy = y as f64 / height as f64;
        let aspecto = width as f64 / height as f64;

        let v1 = self.frente * self.focal;
        let v2 = self.direita * xx * aspecto;
        let v3 = self.up * yy;

        let mut dir = (v1 + v2 + v3).normalizar(1.0);

        //dir.x = -dir.x;
        dir.y = -dir.y;

        Marchador {
            posicao: self.pos,
            direcao: dir.normalizar(1.0),
        }
    }

    fn atualizar(&mut self) {
        let seno_teta = self.teta.sin();
        let cos_teta = self.teta.cos();
        let seno_phi = self.phi.sin();
        let cos_phi = self.phi.cos();

        self.frente =
            Ponto::novo(cos_phi * seno_teta, -seno_phi, cos_phi * cos_teta).normalizar(1.0);
        self.direita = Ponto::novo(cos_teta, 0.0, -seno_teta).normalizar(1.0);
        self.up = cross(self.frente, self.direita).normalizar(1.0);
    }

    pub fn rot_esquerda_direita(&mut self, diff: f64) {
        self.teta += diff;
        self.atualizar();
    }

    pub fn rot_cima_baixo(&mut self, diff: f64) {
        self.phi -= diff;
        self.atualizar();
    }

    pub fn mover_frente_tras(&mut self, diff: f64) {
        self.pos.z += diff;
        self.atualizar();
    }

    pub fn mover_esquerda_direita(&mut self, diff: f64) {
        self.pos.x -= diff;
        self.atualizar();
    }

    pub fn resetar(&mut self) {
        let pos = Ponto::novo(0.0, 1.0, 4.0);
        let focal = 1 as f64;
        let teta = 0 as f64;
        let phi = 0 as f64;

        let seno_teta = teta.sin();
        let cos_teta = teta.cos();
        let seno_phi = phi.sin();
        let cos_phi = phi.cos();

        let frente = Ponto::novo(cos_phi * seno_teta, -seno_phi, cos_phi * cos_teta);
        let direita = Ponto::novo(cos_teta, 0.0, -seno_teta);
        let up = cross(frente, direita).normalizar(1.0);

        *self = Camera {
            pos,
            up,
            frente,
            direita,
            teta,
            phi,
            focal,
        };
    }

    pub fn pos(&self) -> Ponto {
        self.pos
    }
}
