use std::ops::{Add, Mul, Sub};

#[derive(Copy, Clone, Debug)]
pub struct Ponto {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Ponto {
    pub fn novo(x: f64, y: f64, z: f64) -> Ponto {
        Ponto { x, y, z }
    }

    pub fn tamanho(&self) -> f64 {
        self.norma().sqrt()
    }

    pub fn norma(&self) -> f64 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn normalizar(&mut self, escala: f64) -> Ponto {
        if self.tamanho() == 1.0 {
            return self.clone();
        }

        let inv_len = self.tamanho().recip();
        self.x = self.x * inv_len / escala;
        self.y = self.y * inv_len / escala;
        self.z = self.z * inv_len / escala;

        return self.clone();
    }

    pub fn rot_x(&mut self, graus: f64) {
        let seno = graus.sin();
        let coss = graus.cos();

        self.y = self.y * coss - self.z * seno;
        self.z = self.y * seno + self.z * coss;
    }
    pub fn rot_y(&mut self, graus: f64) {
        let seno = graus.sin();
        let coss = graus.cos();

        self.x = self.x * coss + self.z * seno;
        self.z = -self.x * seno + self.z * coss;
    }
    pub fn rot_z(&mut self, graus: f64) {
        let seno = graus.sin();
        let coss = graus.cos();

        self.x = self.x * coss - self.y * seno;
        self.y = self.x * seno + self.y * coss;
    }
}

impl Add for Ponto {
    type Output = Ponto;

    fn add(self, other: Ponto) -> Ponto {
        Ponto {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub for Ponto {
    type Output = Ponto;

    fn sub(self, other: Ponto) -> Ponto {
        Ponto {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Mul<f64> for Ponto {
    type Output = Ponto;

    fn mul(self, other: f64) -> Ponto {
        Ponto {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other,
        }
    }
}

impl Mul<Ponto> for f64 {
    type Output = Ponto;

    fn mul(self, other: Ponto) -> Ponto {
        Ponto {
            x: self * other.x,
            y: self * other.y,
            z: self * other.z,
        }
    }
}
