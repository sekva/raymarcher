use crate::ponto::*;

#[derive(Copy, Clone, Debug)]
pub struct Material {
    pub cor: Ponto,
}

impl Material {
    pub fn branco() -> Material {
        Material {
            cor: Ponto {
                x: 1.0,
                y: 1.0,
                z: 1.0,
            },
        }
    }
    pub fn preto() -> Material {
        Material {
            cor: Ponto {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
        }
    }

    pub fn verde() -> Material {
        Material {
            cor: Ponto {
                x: 0.0,
                y: 1.0,
                z: 0.0,
            },
        }
    }
    pub fn vermelho() -> Material {
        Material {
            cor: Ponto {
                x: 1.0,
                y: 0.0,
                z: 0.0,
            },
        }
    }
}
