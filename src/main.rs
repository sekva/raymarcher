use lazy_static::lazy_static;
use minifb::{Key, Scale, Window, WindowOptions};
use rayon::prelude::*;
use std::sync::{Arc, Mutex};
use std::thread;

use std::sync::mpsc::channel;
use std::sync::mpsc::Receiver;

use rand::seq::SliceRandom;
use rand::thread_rng;

mod ponto;

mod util;
use util::*;

mod marchador;

mod material;

mod elemento;

mod cena;
use cena::*;

mod ray_march;
use ray_march::*;

mod camera;
use camera::*;

const WIDTH: usize = 800 / 1;
const HEIGHT: usize = 600 / 1;

lazy_static! {
    static ref ARRAY: Mutex<Vec<u32>> = Mutex::new(vec![0; WIDTH * HEIGHT]);
    static ref CART: Mutex<Vec<(usize, usize)>> = Mutex::new(Vec::new());
}

fn main() {
    let mut buffer: Vec<u32>; // = vec![0; WIDTH * HEIGHT];

    let mut xs: Vec<usize> = (0..WIDTH).collect();
    let mut ys: Vec<usize> = (0..HEIGHT).collect();

    xs.shuffle(&mut thread_rng());
    ys.shuffle(&mut thread_rng());
    for i in &xs {
        for j in &ys {
            CART.lock().unwrap().push((*i, *j));
        }
    }
    CART.lock().unwrap().shuffle(&mut thread_rng());

    let janela_config = WindowOptions {
        scale: Scale::X1,
        ..WindowOptions::default()
    };

    let mut janela = match Window::new("ESC para fechar", WIDTH, HEIGHT, janela_config) {
        Ok(win) => win,
        Err(err) => {
            println!("Erro na janela: {}", err);
            return;
        }
    };

    let cena = Cena::nova();
    let mut camera = Camera::nova();
    let mov = 0.05;
    camera.rot_esquerda_direita(-mov * 7.0);
    camera.rot_cima_baixo(-mov);

    let mut reset_iter = false;
    let (mut tx, mut rx) = channel();

    let cena_clone = cena.clone();

    let mut _t = thread::spawn(move || {
        render(rx, camera.clone(), &cena_clone);
    });

    while janela.is_open() && !janela.is_key_down(Key::Escape) {
        {
            buffer = ARRAY.lock().unwrap().clone();
        }
        janela.update_with_buffer(&buffer, WIDTH, HEIGHT).unwrap();

        let mov = 0.5;

        if janela.is_key_down(Key::W) {
            reset_iter = true;
            camera.mover_frente_tras(mov);
        }
        if janela.is_key_down(Key::S) {
            reset_iter = true;
            camera.mover_frente_tras(-mov);
        }
        if janela.is_key_down(Key::A) {
            reset_iter = true;
            camera.mover_esquerda_direita(mov);
        }
        if janela.is_key_down(Key::D) {
            reset_iter = true;
            camera.mover_esquerda_direita(-mov);
        }

        let mov = 0.05;

        if janela.is_key_down(Key::NumPad8) {
            reset_iter = true;
            camera.rot_cima_baixo(-mov);
        }
        if janela.is_key_down(Key::NumPad2) {
            reset_iter = true;
            camera.rot_cima_baixo(mov);
        }
        if janela.is_key_down(Key::NumPad6) {
            reset_iter = true;
            camera.rot_esquerda_direita(mov);
        }
        if janela.is_key_down(Key::NumPad4) {
            reset_iter = true;
            camera.rot_esquerda_direita(-mov);
        }

        if janela.is_key_down(Key::NumPad0) {
            reset_iter = true;
            camera.resetar();
        }

        if reset_iter {
            let _ = tx.send(());

            let (txt, rxt) = channel();
            rx = rxt;
            tx = txt;

            let cena_clone = cena.clone();

            for i in 0..(WIDTH * HEIGHT) {
                ARRAY.lock().unwrap()[i] = 0;
            }

            _t = thread::spawn(move || {
                render(rx, camera.clone(), &cena_clone);
            });
            reset_iter = false;
        }
    }

    let _ = tx.send(());
    let _ = _t.join();
}

fn render(reciever: Receiver<()>, camera: Camera, cena: &Cena) {
    println!("comecou render");

    match reciever.try_recv() {
        Ok(()) => {
            dbg!("render retornou");
            return;
        }

        _ => {}
    }

    let duplas;
    {
        duplas = CART.lock().unwrap().clone();
    }

    let retornar = Arc::new(Mutex::new(false));
    let retornar_clone = retornar.clone();

    thread::spawn(move || {
        let _ = reciever.recv();
        *retornar_clone.lock().unwrap() = true;
    });

    duplas.par_iter().for_each(|(x, y)| {
        if *retornar.lock().unwrap() {
            return;
        }

        let marchador_orig = camera.gerar_marchador(*x, *y, WIDTH, HEIGHT);
        if *retornar.lock().unwrap() {
            return;
        }

        let (marchador, _d, material) = ray_march_material(marchador_orig, &cena);
        if *retornar.lock().unwrap() {
            return;
        }

        let cor = ponto_para_cor(cena.luzes4(marchador, material));
        if *retornar.lock().unwrap() {
            return;
        }

        ARRAY.lock().unwrap()[y * WIDTH + x] = cor;
    });
}
