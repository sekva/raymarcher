use crate::elemento::*;
use crate::marchador::*;
use crate::material::*;
use crate::ponto::*;
use crate::ray_march::*;
use crate::util::*;

#[derive(Clone, Debug)]
pub struct Cena {
    pub elementos: Vec<Elemento>,
    pub luzes: Vec<Ponto>,
}

//
//        let d2 = dist_capsula(
//            p,
//            Ponto {
//                x: -1.0,
//                y: 2.5,
//                z: 15.0,
//            },
//            Ponto {
//                x: -3.0,
//                y: 6.5,
//                z: 15.0,
//            },
//            1.0,
//        );
//

impl Cena {
    pub fn nova() -> Cena {
        let elementos: Vec<Elemento> = vec![
            Elemento::novo_quadrilatero(Quadrilatero {
                a: Ponto {
                    x: -5.0,
                    y: 0.0,
                    z: 20.0,
                },
                b: Ponto {
                    x: -5.0,
                    y: 0.0,
                    z: -1.0,
                },
                c: Ponto {
                    x: 5.0,
                    y: 0.0,
                    z: -1.0,
                },
                d: Ponto {
                    x: 5.0,
                    y: 0.0,
                    z: 20.0,
                },

                material: Material::verde(),
            }),
            Elemento::nova_esfera(Esfera {
                origem: Ponto {
                    x: 0.0,
                    y: 1.5,
                    z: 10.0,
                },
                raio: 0.5,
                material: Material::vermelho(),
            }),
        ];

        let luzes: Vec<Ponto> = vec![
            Ponto {
                x: 3.0,
                y: 5.0,
                z: 6.0,
            },
            //Ponto {
            //    x: -3.0,
            //    y: 5.0,
            //    z: 6.0,
            //},
        ];

        Cena { elementos, luzes }
    }

    pub fn distancia(&self, p: Ponto) -> (f64, Material) {
        let mut d = f64::MAX;
        let mut material = Material::preto();

        for i in 0..self.elementos.len() {
            let teste = self.elementos[i].distancia(p);
            if teste < d {
                d = teste;
                material = self.elementos[i].material();
            }
        }

        return (d, material);
    }

    pub fn normal(&self, p: Ponto) -> Ponto {
        Ponto {
            x: self
                .distancia(Ponto {
                    x: p.x + 0.01,
                    y: p.y,
                    z: p.z,
                })
                .0
                - self
                    .distancia(Ponto {
                        x: p.x - 0.01,
                        y: p.y,
                        z: p.z,
                    })
                    .0,
            y: self
                .distancia(Ponto {
                    x: p.x,
                    y: p.y + 0.01,
                    z: p.z,
                })
                .0
                - self
                    .distancia(Ponto {
                        x: p.x,
                        y: p.y - 0.01,
                        z: p.z,
                    })
                    .0,
            z: self
                .distancia(Ponto {
                    x: p.x,
                    y: p.y,
                    z: p.z + 0.01,
                })
                .0
                - self
                    .distancia(Ponto {
                        x: p.x,
                        y: p.y,
                        z: p.z - 0.01,
                    })
                    .0,
        }
        .normalizar(1.0)
    }

    pub fn luzes(&self, p: Ponto) -> f64 {
        let mut difusa = 0.0;

        let pos_luz = *self.luzes.first().unwrap();

        let direcao_luz = (pos_luz - p).normalizar(1.0);
        let normal = self.normal(p);

        let mut luz = clamp(dot(normal, direcao_luz), 0.0, 1.0) / 1.0; //self.luzes.len() as f64;

        let ponto_corrigido = p.clone() + (normal * 0.01);
        //let ponto_corrigido = p.clone() + (normal * 0.01 * 2.0);

        let (_marchador, dist_sombra_luz) = ray_march(
            Marchador {
                posicao: ponto_corrigido,
                direcao: direcao_luz.clone().normalizar(1.0),
            },
            self,
        );

        if dist_sombra_luz < (pos_luz - p).tamanho() {
            luz *= 0.4;
        }

        difusa += luz;

        return difusa;
    }
    pub fn _luzes2(&self, p: Ponto) -> f64 {
        let pos_luz = self.luzes[0];
        let sun_dir = (pos_luz - p).normalizar(1.0);

        let normal = self.normal(p);
        let ponto_corrigido = p.clone() + (normal * 0.02);

        let mut shadow = clamp(dot(normal, sun_dir), 0.0, 1.0);
        let mut shadow_ray_lenght = 0.01;
        let mut shadow_rad = 0.0;
        let sun_size = 1.0;

        while shadow_rad < 100.0 {
            let test_pos = ponto_corrigido + sun_dir * shadow_ray_lenght;
            let dist_to_object = self.distancia(test_pos).0;
            shadow_ray_lenght += dist_to_object;

            shadow = min(shadow, dist_to_object / (shadow_ray_lenght * sun_size));

            if dist_to_object < 0.01 {
                shadow = 0.0;
                break;
            }

            shadow_rad += dist_to_object;
        }

        return shadow * 1.4;
    }

    pub fn luzes3(&self, marchador: Marchador, material: Material) -> Ponto {
        let mut ta_na_sombra = false;

        let mut luz_total = 0.0;

        let p = marchador.posicao;

        for luz in &self.luzes {
            let pos_luz = *luz;
            let direcao_luz = (pos_luz - p).normalizar(1.0);
            let normal = self.normal(p);
            let mut luz = clamp(dot(normal, direcao_luz), 0.0, 1.0) / self.luzes.len() as f64;
            let ponto_corrigido = p.clone() + (normal * 0.01 * 2.0);
            let (_marchador, dist_sombra_luz) = ray_march(
                Marchador {
                    posicao: ponto_corrigido,
                    direcao: direcao_luz.clone().normalizar(1.0),
                },
                self,
            );

            if dist_sombra_luz < (pos_luz - p).tamanho() {
                ta_na_sombra = true;
                luz *= 0.4;
            }

            if !ta_na_sombra {
                let angulo = (dot(normal, direcao_luz)
                    / (normal.tamanho() * direcao_luz.tamanho()))
                .acos()
                .to_degrees();

                if angulo < 90.0 {
                    let lm = direcao_luz.clone().normalizar(1.0);
                    let n = normal.clone().normalizar(1.0);
                    let rm = (2.0 * dot(lm, n) * n - lm).normalizar(1.0);
                    let qtd = dot(rm, (-1.0 * marchador.clone().direcao).normalizar(1.0))
                        .abs()
                        .powf(6.0)
                        / 4.0;
                    luz += qtd;
                }
            }

            luz_total += luz;
        }
        return clamp(luz_total, 0.0, 1.0) * material.cor;
    }

    pub fn luzes4(&self, marchador: Marchador, material: Material) -> Ponto {
        let mut ta_na_sombra = false;

        let mut luz_total = 0.0;

        let p = marchador.posicao;

        for luz in &self.luzes {
            let pos_luz = *luz;
            let direcao_luz = (pos_luz - p).normalizar(1.0);
            let normal = self.normal(p);
            let ponto_corrigido = p.clone() + (normal * 0.01 * 2.0);
            let (_marchador, dist_sombra_luz) = ray_march(
                Marchador {
                    posicao: ponto_corrigido,
                    direcao: direcao_luz.clone().normalizar(1.0),
                },
                self,
            );

            let mut luz = clamp(dot(normal, direcao_luz), 0.0, 1.0)
                * self.sombra(marchador.posicao, pos_luz)
                / self.luzes.len() as f64;
            if dist_sombra_luz < (pos_luz - p).tamanho() {
                ta_na_sombra = true;
                luz *= 0.4;
            }

            if !ta_na_sombra {
                let angulo = (dot(normal, direcao_luz)
                    / (normal.tamanho() * direcao_luz.tamanho()))
                .acos()
                .to_degrees();

                if angulo < 90.0 {
                    let lm = direcao_luz.clone().normalizar(1.0);
                    let n = normal.clone().normalizar(1.0);
                    let rm = (2.0 * dot(lm, n) * n - lm).normalizar(1.0);
                    let qtd = dot(rm, (-1.0 * marchador.clone().direcao).normalizar(1.0))
                        .abs()
                        .powf(6.0)
                        / 4.0;
                    luz += qtd;
                }
            }

            luz_total += luz;
        }
        return clamp(luz_total, 0.0, 1.0) * material.cor;
    }

    pub fn sombra(&self, ro: Ponto, rd: Ponto) -> f64 {
        let mint = 0.01;
        let tmax = 3.0;
        let mut res = 1.0;
        let mut t = mint;
        let mut ph = 1e10; // big, such that y = 0 on the first iteration

        for _ in 0..32 {
            let h = self.distancia(ro + (rd * t)).0;

            res = min(res, 10.0 * h / t);

            // use this if you are getting artifact on the first iteration, or unroll the
            // first iteration out of the loop
            //float y = (i==0) ? 0.0 : h*h/(2.0*ph);

            //let y = h * h / (2.0 * ph);
            //let d = (h * h - y * y).sqrt();
            //res = min(res, 10.0 * d / max(0.0, t - y));
            //ph = h;

            //t += h*.8+.01;
            t += h;

            if res < 0.0001 || t > tmax {
                break;
            }
        }
        return clamp(res, 0.0, 1.0);
    }
}
