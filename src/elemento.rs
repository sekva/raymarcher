use crate::material::*;

pub trait ElementoT {
    fn distancia(&self, p: Ponto) -> f64;
    fn material(&self) -> Material;
}

//pub type Elemento = Box<dyn ElementoT>;

use crate::ponto::*;
use crate::util::*;

#[derive(Copy, Clone, Debug)]
pub struct Esfera {
    pub origem: Ponto,
    pub raio: f64,
    pub material: Material,
}

#[derive(Copy, Clone, Debug)]
pub struct Capsula {
    pub a: Ponto,
    pub b: Ponto,
    pub raio: f64,
    pub material: Material,
}
#[derive(Copy, Clone, Debug)]
pub struct Quadrilatero {
    pub a: Ponto,
    pub b: Ponto,
    pub c: Ponto,
    pub d: Ponto,
    pub material: Material,
}

#[derive(Copy, Clone, Debug)]
pub enum TiposElementos {
    _ESFERA(Esfera),
    _CAPSULA(Capsula),
    _QUADRILATERO(Quadrilatero),
}

#[derive(Copy, Clone, Debug)]
pub struct Elemento {
    elemento: TiposElementos,
}

impl Elemento {
    pub fn novo_quadrilatero(e: Quadrilatero) -> Elemento {
        Elemento {
            elemento: TiposElementos::_QUADRILATERO(e),
        }
    }
    pub fn _nova_capsusa(e: Capsula) -> Elemento {
        Elemento {
            elemento: TiposElementos::_CAPSULA(e),
        }
    }
    pub fn nova_esfera(e: Esfera) -> Elemento {
        Elemento {
            elemento: TiposElementos::_ESFERA(e),
        }
    }
}

impl ElementoT for Elemento {
    fn distancia(&self, p: Ponto) -> f64 {
        match self.elemento {
            TiposElementos::_ESFERA(e) => e.distancia(p),
            TiposElementos::_CAPSULA(e) => e.distancia(p),
            TiposElementos::_QUADRILATERO(e) => e.distancia(p),
        }
    }
    fn material(&self) -> Material {
        match self.elemento {
            TiposElementos::_ESFERA(e) => e.material(),
            TiposElementos::_CAPSULA(e) => e.material(),
            TiposElementos::_QUADRILATERO(e) => e.material(),
        }
    }
}

impl ElementoT for Esfera {
    fn distancia(&self, p: Ponto) -> f64 {
        (p - self.origem).tamanho() - self.raio
    }

    fn material(&self) -> Material {
        return self.material.clone();
    }
}
impl ElementoT for Capsula {
    fn distancia(&self, p: Ponto) -> f64 {
        let ab = self.b - self.a;
        let ap = p - self.a;

        let t = clamp(dot(ab, ap) / dot(ab, ab), 0.0, 1.0);

        let c = self.a + (ab * t);

        let r = p - c;

        return r.tamanho() - self.raio;
    }

    fn material(&self) -> Material {
        return self.material.clone();
    }
}
impl ElementoT for Quadrilatero {
    fn distancia(&self, p: Ponto) -> f64 {
        let ba = self.b - self.a;
        let pa = p - self.a;
        let cb = self.c - self.b;
        let pb = p - self.b;
        let dc = self.d - self.c;
        let pc = p - self.c;
        let ad = self.a - self.d;
        let pd = p - self.d;
        let nor = cross(ba, ad);

        let t = sign(dot(cross(ba, nor), pa))
            + sign(dot(cross(cb, nor), pb))
            + sign(dot(cross(dc, nor), pc))
            + sign(dot(cross(ad, nor), pd));

        if t < 3.0 {
            return (min(
                min(
                    min(
                        dot2(ba * clamp(dot(ba, pa) / dot2(ba), 0.0, 1.0) - pa),
                        dot2(cb * clamp(dot(cb, pb) / dot2(cb), 0.0, 1.0) - pb),
                    ),
                    dot2(dc * clamp(dot(dc, pc) / dot2(dc), 0.0, 1.0) - pc),
                ),
                dot2(ad * clamp(dot(ad, pd) / dot2(ad), 0.0, 1.0) - pd),
            ))
            .sqrt();
        }

        return (dot(nor, pa) * dot(nor, pa) / dot2(nor)).sqrt();
    }

    fn material(&self) -> Material {
        return self.material.clone();
    }
}
