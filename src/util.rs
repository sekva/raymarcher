use crate::ponto::*;

pub fn dot(a: Ponto, b: Ponto) -> f64 {
    a.x * b.x + a.y * b.y + a.z * b.z
}

pub fn dot2(p: Ponto) -> f64 {
    dot(p, p)
}

pub fn cross(a: Ponto, b: Ponto) -> Ponto {
    Ponto {
        x: a.y * b.z - a.z * b.y,
        y: a.z * b.x - a.x * b.z,
        z: a.x * b.y - a.y * b.x,
    }
}

pub fn sign(v: f64) -> f64 {
    if v > 0.0 {
        return 1.0;
    }

    if v < 0.0 {
        return -1.0;
    }

    return 0.0;
}

pub fn clamp(v: f64, lo: f64, hi: f64) -> f64 {
    if v < lo {
        return lo;
    }

    if v > hi {
        return hi;
    }

    return v;
}

pub fn min(a: f64, b: f64) -> f64 {
    if a < b {
        return a;
    }

    return b;
}

pub fn max(a: f64, b: f64) -> f64 {
    if a > b {
        return a;
    }

    return b;
}

pub fn float_para_byte(f: f64) -> u8 {
    let v = max(0.0, min(1.0, f));
    (v * 254.0) as u8
}

pub fn ponto_para_cor(p: Ponto) -> u32 {
    let r = float_para_byte(p.x) as u32;
    let g = float_para_byte(p.y) as u32;
    let b = float_para_byte(p.z) as u32;

    (r << 16) | (g << 8) | (b)
}

pub fn _map(value: f64, istart: f64, istop: f64, ostart: f64, ostop: f64) -> f64 {
    return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

//////////////////////////////////////

pub fn _dist_capsula(p: Ponto, a: Ponto, b: Ponto, raio: f64) -> f64 {
    let ab = b - a;
    let ap = p - a;

    let t = clamp(dot(ab, ap) / dot(ab, ab), 0.0, 1.0);

    let c = a + (ab * t);

    let r = p - c;

    return r.tamanho() - raio;
}

pub fn _dist_esfera(ponto: Ponto, centro_da_esfera: Ponto, raio: f64) -> f64 {
    (ponto - centro_da_esfera).tamanho() - raio
}

pub fn _dist_quadrilatero(p: Ponto, a: Ponto, b: Ponto, c: Ponto, d: Ponto) -> f64 {
    let ba = b - a;
    let pa = p - a;
    let cb = c - b;
    let pb = p - b;
    let dc = d - c;
    let pc = p - c;
    let ad = a - d;
    let pd = p - d;
    let nor = cross(ba, ad);

    let t = sign(dot(cross(ba, nor), pa))
        + sign(dot(cross(cb, nor), pb))
        + sign(dot(cross(dc, nor), pc))
        + sign(dot(cross(ad, nor), pd));

    if t < 3.0 {
        return (min(
            min(
                min(
                    dot2(ba * clamp(dot(ba, pa) / dot2(ba), 0.0, 1.0) - pa),
                    dot2(cb * clamp(dot(cb, pb) / dot2(cb), 0.0, 1.0) - pb),
                ),
                dot2(dc * clamp(dot(dc, pc) / dot2(dc), 0.0, 1.0) - pc),
            ),
            dot2(ad * clamp(dot(ad, pd) / dot2(ad), 0.0, 1.0) - pd),
        ))
        .sqrt();
    }

    return (dot(nor, pa) * dot(nor, pa) / dot2(nor)).sqrt();
}

pub fn _dist_para_cena(ponto: Ponto) -> f64 {
    let d1 = _dist_esfera(
        ponto,
        Ponto {
            x: 0.0,
            y: 2.5,
            z: 15.0,
        },
        0.5,
    );

    let d2 = _dist_capsula(
        ponto,
        Ponto {
            x: -1.0,
            y: 2.5,
            z: 15.0,
        },
        Ponto {
            x: -3.0,
            y: 6.5,
            z: 15.0,
        },
        1.0,
    );

    return min(d2, min(d1, ponto.y));
}
