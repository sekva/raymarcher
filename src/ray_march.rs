use crate::cena::*;
use crate::marchador::*;
use crate::material::*;
use crate::util::*;

pub fn ray_march(mut marchador: Marchador, cena: &Cena) -> (Marchador, f64) {
    let mut comulativa = 0.0;
    for _ in 0..100 {
        let dist = cena.distancia(marchador.posicao).0;
        if dist < 0.01 {
            break;
        //return comulativa + dist;
        } else if comulativa > 100.0 {
            break;
        } else {
            comulativa += dist;
            marchador.mover(dist);
        }
    }

    return (marchador, comulativa);
}

pub fn ray_march_material(mut marchador: Marchador, cena: &Cena) -> (Marchador, f64, Material) {
    let mut comulativa = 0.0;
    let mut material = Material::branco();
    for _ in 0..100 {
        let (dist, material2) = cena.distancia(marchador.posicao);
        if dist < 0.01 {
            material = material2;
            break;
        //return comulativa + dist;
        } else if comulativa > 100.0 {
            break;
        } else {
            comulativa += dist;
            marchador.mover(dist);
        }
        material = Material::preto();
    }

    return (marchador, comulativa, material);
}

pub fn _ray_march_sombra(mut marchador: Marchador, cena: &Cena, w: f64) -> (Marchador, f64) {
    let mut s = 1.0;
    let mut t = 0.0;
    while t < 100.0 {
        let h = cena.distancia(marchador.posicao + marchador.direcao * t).0;
        s = min(s, 0.5 + 0.5 * h / (w * t));
        if s < 0.0 {
            break;
        }
        t += h;
    }
    s = max(s, 0.0);
    marchador.posicao = marchador.posicao + marchador.direcao * t;
    return (marchador, s * s * (3.0 - 2.0 * s)); // smoothstep

    //let mut res = 1.0;
    //let mut ph = 1e20 as f64;

    //let mut t = 0.0;

    //while t < 100.0 {
    //    let h = cena.distancia(marchador.posicao + marchador.direcao * t);
    //    if h < 0.001 {
    //        marchador.posicao = marchador.posicao + marchador.direcao * t;
    //        return (marchador, 0.0);
    //    }
    //    let y = h * h / (2.0 * ph);
    //    let d = (h * h - y * y).sqrt();
    //    res = min(res, w * d / max(0.0, t - y));
    //    ph = h;
    //    t += h;
    //}
    //marchador.posicao = marchador.posicao + marchador.direcao * t;
    //return (marchador, res);
}
