use crate::ponto::*;

#[derive(Copy, Clone, Debug)]
pub struct Marchador {
    pub posicao: Ponto,
    pub direcao: Ponto,
}

impl Marchador {
    pub fn mover(&mut self, quantidade: f64) {
        self.posicao.x += self.direcao.x * quantidade;
        self.posicao.y += self.direcao.y * quantidade;
        self.posicao.z += self.direcao.z * quantidade;
    }
}
